# Analysing popular android obfuscation methods

## Contents

- [Analysing popular android obfuscation methods](#analysing-popular-android-obfuscation-methods)
    - [Contents](#contents)
    - [Introduction](#introduction)
    - [Popular obfuscation tools](#popular-obfuscation-tools)
    - [Popular deobfuscation tools](#popular-deobfuscation-tools)
    - [Popular obfuscation methods](#popular-obfuscation-methods)
        - [Reflection](#reflection)
        - [String encryption](#string-encryption)
        - [Pure function calls](#pure-function-calls)
        - [Useless fields and deadcode](#useless-fields-and-deadcode)
        - [Native code execution](#native-code-execution)
        - [Overloading abuse](#overloading-abuse)
    - [My approach](#my-approach)
    - [References](#references)

*Abstract - A*

*Keywords - Deobfuscation; Dalvik; Android*

## I.Introduction

Android package security is lacking of code-obfuscation tools. Few of them used as code-base for proprietary solutions. The same goes for deobfuscation. Nowadays analyzing the android package one can face the difficulties reading the apk code.
<!-- 
### Warning! The article below contains mostly of code. Be ready to read lots of useless and dumb code
-->
## Popular obfuscation tools

- Free or open-source
    - [yGuard](https://www.yworks.com/products/yguard)
    - [ProGuard](https://www.guardsquare.com/proguard)
    - [Obfuscapk](https://github.com/ClaudiuGeorgiu/Obfuscapk)
    - [Obfuscator](https://github.com/superblaubeere27/obfuscator)
- Proprietary
    - [DexGuard](https://www.guardsquare.com/dexguard)
    - [ApiGuard](https://www.f5.com)

## Popular deobfuscation tools

- Free or open-source
    - [Deoptfuscator](https://github.com/Gyoonus/deoptfuscator)
        - Pros

            - Deobfuscates control flow graphs protected by DexGuard
        
        - Cons

            - Only optimizes CFGs

        ### Wasn't tested

    - [Simplify](https://github.com/CalebFenton/simplify)
        - Pros

            - Traces everything inside virtual environment
            - Has lots of oportunities to enhance the deobfuscation quality and process

        - Cons

            - Very Slow (149 methods were processing over 2 days)
            - Can't process strongly obfuscated methods
            - Wont call android sdk methods

        ### Result on test apk

        149 methods were processed
        - Nothing were deobfuscated properly
             
        
    - [Dex-oracle](https://github.com/CalebFenton/dex-oracle)
        - Pros

            - Easily customizable
            - Has lots of oportunities to enhance the deobfuscation quality and process

        - Cons

            - Don't have much open-source extensions
            - Has less opportuinites of tracing arbitrary code than simplify
            - It's just analyzing the smali code using regular expression patterns
            - Wont optimize control flow graph

        ### Result on test apk

        1791 methods were processed
        - Only few strings were decrypted
        - Only few methods were unreflected

## Popular obfuscation methods

### Reflection

Arbitrary app can exploit language features to inspect, instantiate objects and invoke methods by name at runtime, making end-up code almost unreadable in combination with other obfuscation methods such as string encryption. 

Before

```java
boolean isRunning = android.os.SystemProperties.get("init.svc.su_daemon", "status").equals("running");
```
After

```java
boolean isRunning = ((String) Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class, String.class}).invoke((Object) null, new Object[]{"init.svc.su_daemon", "status"})).equals("running");
```


### String encryption

Arbitrary app can contain encrypted const strings with decrypt stub calls, instead of straight access. Decryption stub always change, so it's possible to have lots of different decryption stubs inside a single class definition.

Before

```java
String someString = "init.svc.su_daemon";
```

After

```java
String someString = getBody("\u08f9\udd1a\ud9e1\uadf6\u4e8f\u95f1\u4891\u4ab4\ubf3c\u8377\u63b6\u2abf\ud8ee\ub47a\u974d\u89e6");
```

### Pure function calls

Arbitrary app can contain static or virtual function calls with identical result and no side effects for identical arguments. 

Before 

```java
int someInt = 123;
```

After

```java
int zero = 123 + 
    // always 0
    android.graphics.Color.red(0) - 
    // always 0
    android.text.TextUtils.getOffsetBefore("", 0) + 
    // always 0
    android.graphics.Color.argb(0, 0, 0, 0);
```

### Useless fields and deadcode

Arbitrary app can contain wanton code with no side effects on the original code. For example opaque predicates (conditional branches with permanent false or true result), sometimes being followed by code which causes termination of program or unresolved errors. 

Before

```java
if (!getBody()) {
    return;
}

throw new Exception();
```

After

```java
private static int AGRequestBuilder = 0; // Useless, always even
private static int newBuilder = 1;       // Useless, always odd

...

int i = AGRequestBuilder + 93;
// newBuilder is always odd
newBuilder = i % 128;

// Opaque predicate
// 1 == 0
if (!(i % 2 != 0)) {
    // Useless code which would never be executed
    // It can generate unhandled exceptions etc.
    boolean method = setOnDragListener.getMethod();
    java.lang.Object obj = null;
    super.hashCode();
}

if (getBody()) {
    throw new Exception();
}

int i2 = newBuilder + 99;
// AGRequestBuilder is always even
AGRequestBuilder = i2 % 128;
// Useless
int i3 = i2 % 2;
```

### Native code execution

Arbitrary app can contain native calls to compiled C-code packaged as shared libraries. Which are being loaded using `System.loadLibrary(libName)`. Basically it provides additional application security. Shared libraries may also contain obfuscated asm code.

### Overloading abuse

Arbitrary app can contain fields and methods with the same name. This will turn in as most of the method calls will appear with same name and different signatures, so person will find it difficult to read some code pieces.

Before

```java
public final class Class1 {
    private static final long[] field1;
    private static final long[] field2;
    private static int method7(int i, String str);
    private static int method8(int i);
}
```

After

```java
public final class getHeaders {
    private static final long[] setWillNotDraw;
    private static final long[] setHapticFeedbackEnabled;   
    private static int getHeaders(int i, String str);
    private static int getHeaders(int i);
}
```

## My approach
<!-- 
### **Warning! Code below is posted only as PoC(proof of concept). I don't recommend mindlessly pasting it inside your project or using it before you understand it's meaning. I've never written on D before, so if you are D senior developer, god please don't read my code or treat it seriously.**

<br>
-->
I liked the way dex-oracle used the driver to solve pure function calls. So I've focused on extending its functionality, instead of writing my own deobfuscator from scratch.

The first problem I encountered was decompiling dalvik executable (DEX) from android application package file (APK).

Basically APK is a simple zip archive, so there's no need to worry about it's unpacking. The detailed DEX file format paper is published on official android website. [2]

The next problem was utilizing useless code instructions and optimizing arithmetic operations. Basically this action is the same as creating simple virtual machine for instruction tracing. So I've written simple smali tracing virtual machine.

```d
// smali_tracer.d
class SmaliTracer {

    this();

    void parse_arithmetics(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_move_result(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_instance_op(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_comparison(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_invocation(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_exception_op(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_switch_op(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_static_op(Dex dex, ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_array_op(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_conditional(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_const(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void parse_cast(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx);

    void determine_action(Dex dex, ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx) {

        switch(inst.type) {

            case SMALI_TYPES.TYPE_ARITHMETICS:
                parse_arithmetics(d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_MOVE_RESULT:
                parse_move_result(d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_INSTANCE_OP:
                parse_instance_op(d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_COMPARISON:
                parse_comparison(d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_INVOCATION:
                parse_invocation(d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_EXCEPTION:
                parse_exception_op(d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_SWITCH_OP:
                parse_switch_op(d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_STATIC_OP:
                parse_static_op(dex, d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_ARRAY_OP:
                parse_array_op(d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_RETURN:
                idx = cast(uint)(method.instructions.length);
                break;

            case SMALI_TYPES.TYPE_MOVE:
                registers[inst.arguments[0]] = registers.get(
                    inst.arguments[1], SmaliRegister.NULL());
                break;
            
            case SMALI_TYPES.TYPE_CONST:
                parse_const(d, cls, method, inst, idx);
                break;

            case SMALI_TYPES.TYPE_COND:
                parse_conditional(d, cls, method, inst, idx);
                break;
                
            case SMALI_TYPES.TYPE_GOTO:
                idx = cast(int)(inst.arguments[$ - 1]);
                break;

            case SMALI_TYPES.TYPE_CAST:
                parse_cast(d, cls, method, inst, idx);
                break;
                
            default:
                break;
        }
    }

    bool trace(Dex dex, ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method) {

        auto modified = false;
        for(int i = 0; i < method.instructions.length; ++i) {
            auto inst = method.instructions[i];

            if(inst.visited)
                break;

            inst.visit();
            determine_action(dex, d, cls, method, inst, i);
        }

        return modified;
    }

    protected SmaliValue result;
    protected SmaliRegister[ulong] registers;
}
```

To make instruction tracing more comfortable I've created corresponding classes for instruction and register.

```d
// smali_instruction.d

struct PackedPayload {
    ushort magic;
    uint size;
    uint element_size;
    ubyte*[int] elements;
    uint size_in_bytes;
    uint offset;
}

class SmaliInstruction {

    this(const ubyte* operands, ref uint offset) {

        this.payload.magic = 0x0;
        this.bytecode = operands[offset++];

        parse(operands, offset);

        assert(!(offset & 1));
    }

    private void parse(const ubyte* operands, ref uint offset);

    public void visit() {
        this.visited = true;
    }

    public bool has_jump() {

        return this.type = SMALI_TYPES.TYPE_COND ||
            this.type == SMALI_TYPES.TYPE_GOTO;
    }

    ubyte type;
    ushort bytecode;
    public bool visited;
    public ulong[] arguments;
    public PackedPayload payload;
}
```

```d
// smali_register.d
class SmaliRegister {

    this(SmaliValue value, bool paired = false) {

        this.value = value;
        this.paired = paired;
    }

    static this() {
        null_register = new SmaliRegister(
            new NullValue()
        );
    }

    static ref SmaliRegister NULL() {
        return null_register;
    }
    private static SmaliRegister null_register;
    public SmaliValue value;
    public bool paired;
}
```

Then I've written the SmaliMethod class to parse it's instructions.

```d
// smali_method.d

class SmaliMethod {

    this(string class_name, string name, string prototype,
        string return_type, ref ushort[] instructions,
        bool is_native = false);

    this(string class_name, string signature);

    public void parse_body();

    protected ushort[] body;

    public string name;
    
    public string signature;
    public string descriptor;
    public string return_type;

    public string[] parameters;
    public SmaliInstruction[] instructions;

    public string class_name;

    public bool modified;
    public bool is_native;
}
```

For now we need to iterate over methods instructions and handle them as computer does. Here's the example of my code for handling _const-family_ instructions

```d
void parse_const(ref Dessector d, ref SmaliClass cls,
        ref SmaliMethod method, ref SmaliInstruction inst, ref int idx) {

  switch(inst.bytecode) {

      case SMALI_OPCODES.const_4:
      case SMALI_OPCODES.const_16:
      case SMALI_OPCODES.const_regular:
          registers[inst.arguments[0]] = new SmaliRegister(
              new UnidentifiedValue!(32)(
                  cast(uint)(inst.arguments[1])
              )
          );
          break;
      
      case SMALI_OPCODES.const_wide:
      case SMALI_OPCODES.const_wide_16:
      case SMALI_OPCODES.const_wide_32:
      case SMALI_OPCODES.const_wide_high_16:
          registers[inst.arguments[0]] = new SmaliRegister(
              new UnidentifiedValue!(64)(inst.arguments[1]), true
          );
          break;

      case SMALI_OPCODES.const_string:
      case SMALI_OPCODES.const_string_jumbo:
          registers[inst.arguments[0]] = new SmaliRegister(
              new StringValue(d.string_defs[inst.arguments[1]].str_data)
          );
          break;
      default:
          assert(false);
  }
}
```

Here's the example of processing over 1791 methods with additional const-calculation and junk field sanitization.

Before 

```java
static {
    int i = 0 + 53;
    getBody = i % 128;
    if ((i % 2 == 0 ? '7' : 18) != 18) {
        Object obj = null;
        super.hashCode();
    }
}
```

After

```java
static {
    if ((1 == 0 ? '7' : 18) != 18) {
        Object obj = null;
        super.hashCode();
    }
}
```

Well, it looks good, but may be even better. Let's solve constant evaluated conditional branches to get rid of opaque predicates.

Result

```java
static { }
```

For now this method is empty, because simplifier replaced conditional branch with goto, but that's only particular case and overall code still looks quite ugly, here's another example.

Before simplifcation 

```java
static {
    char[] cArr = new char[1317];
    java.nio.ByteBuffer.wrap("......".getBytes("ISO-8859-1")).asCharBuffer().get(cArr, 0,1317);
    getStatusCode = cArr;
    long[] jArr = { ... };
    getMethod = jArr;
    long[] jArr2 = { ... };
    getHeaders = jArr2;
    long[] jArr3 = { ... };
    getBody = jArr3;
    long[] jArr4 = { ... };
    AGRequest$Builder = jArr4;
    long[] jArr5 = { ... };
    addHeader = jArr5;
    long[] jArr6 = { ... };
    newBuilder = jArr6;
    int scrollDefaultDelay = android.view.ViewConfiguration.getScrollDefaultDelay() >> 16;
    int i = -android.graphics.Color.blue(0);
    int i2 = (android.view.ViewConfiguration.getScrollFriction() > 0.0f ? 1 : (android.viewViewConfiguration.getScrollFriction() == 0.0f ? 0 : -1));
    int i3 = -android.text.TextUtils.getOffsetBefore("", 0);
    int i4 = -(android.os.Process.myPid() >> 22);
    int i5 = (android.view.ViewConfiguration.getScrollFriction() > 0.0f ? 1 : (android.viewViewConfiguration.getScrollFriction() == 0.0f ? 0 : -1));
    int i6 = -android.text.TextUtils.indexOf("", '0', 0, 0);
    int i7 = -(android.os.Process.myPid() >> 22);
    int i8 = -android.text.TextUtils.lastIndexOf("", '0', 0, 0);
    int i9 = -(-(android.view.ViewConfiguration.getFadingEdgeLength() >> 16));
    int i10 = (i9 & 1201) + (i9 | 1201);
    int i11 = -(android.view.ViewConfiguration.getMinimumFlingVelocity() >> 16);
    int i12 = -(-(android.view.ViewConfiguration.getJumpTapTimeout() >> 16));
    int i13 = -(-android.widget.ExpandableListView.getPackedPositionChild(0));
    int i14 = -(-(android.view.ViewConfiguration.getMaximumFlingVelocity() >> 16));
    int i15 = (i14 ^ 1242) + ((i14 & 1242) << 1);
    int i16 = -android.view.KeyEvent.getDeadChar(0, 0);
    url = new java.lang.String[]{getHeaders((char) (((scrollDefaultDelay | 44643) << 1) -(scrollDefaultDelay ^ 44643)), 1100 - android.view.View.MeasureSpec.makeMeasureSpec(0, 0, (i & 12) + (i | 12)), getHeaders((char) ((i2 & -1) + (i2 | -1)), (android.viewViewConfiguration.getScrollFriction() > 0.0f ? 1 : (android.view.ViewConfigurationgetScrollFriction() == 0.0f ? 0 : -1)) + 1111, (i3 & 16) + (i3 | 16)), getHeaders((char)(android.view.ViewConfiguration.getLongPressTimeout() >> 16), (i4 & 1128) + (i4 | 1128),17 - (android.view.ViewConfiguration.getScrollBarSize() >> 8)), getHeaders((char) ((i5 &-1) + (i5 | -1)), (1145 - (~(-(android.media.AudioTrack.getMinVolume() > 0.0f ? 1 :(android.media.AudioTrack.getMinVolume() == 0.0f ? 0 : -1))))) - 1, (i6 & 5) + (i6 | 5), getHeaders((char) ((36367 - (~android.graphics.Color.red(0))) - 1), (1151 - ((-android.view.View.getDefaultSize(0, 0)))) - 1, ((i7 | 12) << 1) - (i7 ^ 12)),getHeaders((char) (android.util.TypedValue.complexToFraction(0, 0.0f, 0.0f) > 0.0f ? 1 :(android.util.TypedValue.complexToFraction(0, 0.0f, 0.0f) == 0.0f ? 0 : -1)), ((i8 |1162) << 1) - (i8 ^ 1162), 17 - android.view.View.resolveSize(0, 0)), getHeaders((char) (5939 - (~android.widget.ExpandableListView.getPackedPositionGroup(0))) - 1), 1180 -(android.util.TypedValue.complexToFloat(0) > 0.0f ? 1 : (android.util.TypedValuecomplexToFloat(0) == 0.0f ? 0 : -1)), 21 - android.text.TextUtils.indexOf("", "")),getHeaders((char) android.text.TextUtils.getTrimmedLength(""), i10, ((i11 | 16) << 1) -(i11 ^ 16)), getHeaders((char) (android.view.ViewConfiguration.getEdgeSlop() >> 16),(i12 & 1217) + (i12 | 1217), 25 - (android.view.ViewConfiguration.getFadingEdgeLength()>> 16)), getHeaders((char) ((i13 & 55906) + (i13 | 55906)), i15, (i16 & 13) + (i16 | 13)};
    int i17 = -(android.os.Process.myPid() >> 22);
    int i18 = -android.text.TextUtils.indexOf("", "", 0, 0);
    char c = (char) ((59248 - (~(-android.graphics.Color.red(0)))) - 1);
    int edgeSlop = android.view.ViewConfiguration.getEdgeSlop() >> 16;
    int i19 = (edgeSlop ^ 1285) + ((edgeSlop & 1285) << 1);
    int i20 = -(android.os.SystemClock.elapsedRealtime() > 0 ? 1 : (android.os.SystemClockelapsedRealtime() == 0 ? 0 : -1));
    int threadPriority = android.os.Process.getThreadPriority(0);
    int i21 = -android.text.TextUtils.lastIndexOf("", '0');
    int i22 = -(android.os.Process.myTid() >> 22);
    int i23 = -(-(android.os.SystemClock.currentThreadTimeMillis() > -1 ? 1 : (android.osSystemClock.currentThreadTimeMillis() == -1 ? 0 : -1)));
    int i24 = -(-android.text.TextUtils.indexOf("", '0', 0, 0));
    method = new java.lang.String[]{getHeaders((char) ((i17 ^ 35006) + ((i17 & 35006) << 1), 1255 - android.text.TextUtils.getCapsMode("", 0, 0), 7 - (android.widgetExpandableListView.getPackedPositionForGroup(0) > 0 ? 1 : (android.widgetExpandableListView.getPackedPositionForGroup(0) == 0 ? 0 : -1))), getHeaders((char)android.view.View.getDefaultSize(0, 0), (1261 - (~(-android.text.TextUtils.lastIndexO("", '0', 0)))) - 1, (i18 & 11) + (i18 | 11)), getHeaders((char) (android.viewViewConfiguration.getFadingEdgeLength() >> 16), (android.view.ViewConfigurationgetDoubleTapTimeout() >> 16) + 1273, (android.os.Process.myTid() >> 22) + 12), getHeader(c, i19, (i20 ^ 13) + ((i20 & 13) << 1)), getHeaders((char) android.view.ViewMeasureSpec.makeMeasureSpec(0, 0), (1298 - (~(-(android.view.ViewConfigurationgetZoomControlsTimeout() > 0 ? 1 : (android.view.ViewConfiguration.getZoomControlsTimeou() == 0 ? 0 : -1))))) - 1, ((((threadPriority | 20) << 1) - (threadPriority ^ 20)) >> 6)+ 11), getHeaders((char) ((i21 ^ -1) + ((i21 & -1) << 1)), (1308 - (~(-android.graphicsdrawable.Drawable.resolveOpacity(0, 0)))) - 1, ((i22 | 5) << 1) - (i22 ^ 5)), getHeaders(char) ((i23 & 53763) + (i23 | 53763)), (1313 - (~(-android.text.TextUtilsgetTrimmedLength("")))) - 1, (i24 & 5) + (i24 | 5))};
    long[] jArr7 = { ... };
    put = jArr7;
    head = new setWillNotDraw.getHeaders(40, jArr4, 0, 0);
    get = new setWillNotDraw.getHeaders(20, jArr2, 0, 0);
    delete = new setWillNotDraw.getHeaders(60, jArr6, 0, 0);
    post = new setWillNotDraw.getHeaders(40, jArr3, 5, Long.MAX_VALUE);
    AGResponse = new setWillNotDraw.getHeaders(20, jArr, 5, Long.MAX_VALUE);
    new setWillNotDraw.getHeaders(60, jArr5, 5, Long.MAX_VALUE);
    headersMultiMap = new setWillNotDraw.getHeaders(150, jArr7, 5, 1073741823);
    int i25 = values;
    int i26 = (i25 & 99) + (i25 | 99);
    AGState = i26 % 128;
    if (i26 % 2 == 0) {
        int i27 = statusCode + 107;
        AGResponse$Builder = i27 % 128;
        int i28 = i27 % 2;
        return;
    }
    int i29 = AGResponse$Builder + 69;
    statusCode = i29 % 128;
    int i30 = i29 % 2;
}
```

After

```java
static {
    char[] cArr = new char[1317];
    java.nio.ByteBuffer.wrap("...".getBytes("ISO-8859-1")).asCharBuffer().get(cArr, 0, 1317);
    getStatusCode = cArr;
    long[] jArr = { ... };
    getMethod = jArr;
    long[] jArr2 = { ... };
    getHeaders = jArr2;
    long[] jArr3 = { ... };
    getBody = jArr3;
    long[] jArr4 = { ... };
    AGRequest$Builder = jArr4;
    long[] jArr5 = { ... };
    addHeader = jArr5;
    long[] jArr6 = { ... };
    newBuilder = jArr6;
    int scrollDefaultDelay = android.view.ViewConfiguration.getScrollDefaultDelay() >> 16;
    int i = -android.graphics.Color.blue(0);
    int i2 = (android.view.ViewConfiguration.getScrollFriction() > 0.0f ? 1 : (android.viewViewConfiguration.getScrollFriction() == 0.0f ? 0 : -1));
    int i3 = -android.text.TextUtils.getOffsetBefore("", 0);
    int i4 = -(android.os.Process.myPid() >> 22);
    int i5 = (android.view.ViewConfiguration.getScrollFriction() > 0.0f ? 1 : (android.viewViewConfiguration.getScrollFriction() == 0.0f ? 0 : -1));
    int i6 = -android.text.TextUtils.indexOf("", '0', 0, 0);
    int i7 = -(android.os.Process.myPid() >> 22);
    int i8 = -android.text.TextUtils.lastIndexOf("", '0', 0, 0);
    int i9 = -(-(android.view.ViewConfiguration.getFadingEdgeLength() >> 16));
    int i10 = (i9 & 1201) + (i9 | 1201);
    int i11 = -(android.view.ViewConfiguration.getMinimumFlingVelocity() >> 16);
    int i12 = -(-(android.view.ViewConfiguration.getJumpTapTimeout() >> 16));
    int i13 = -(-android.widget.ExpandableListView.getPackedPositionChild(0));
    int i14 = -(-(android.view.ViewConfiguration.getMaximumFlingVelocity() >> 16));
    int i15 = (i14 ^ 1242) + ((i14 & 1242) << 1);
    int i16 = -android.view.KeyEvent.getDeadChar(0, 0);
    url = new java.lang.String[]{getHeaders((char) (((scrollDefaultDelay | 44643) << 1) -(scrollDefaultDelay ^ 44643)), 1100 - android.view.View.MeasureSpec.makeMeasureSpec(0, 0, (i & 12) + (i | 12)), getHeaders((char) ((i2 & -1) + (i2 | -1)), (android.viewViewConfiguration.getScrollFriction() > 0.0f ? 1 : (android.view.ViewConfigurationgetScrollFriction() == 0.0f ? 0 : -1)) + 1111, (i3 & 16) + (i3 | 16)), getHeaders((char)(android.view.ViewConfiguration.getLongPressTimeout() >> 16), (i4 & 1128) + (i4 | 1128),17 - (android.view.ViewConfiguration.getScrollBarSize() >> 8)), getHeaders((char) ((i5 &-1) + (i5 | -1)), (1145 - (~(-(android.media.AudioTrack.getMinVolume() > 0.0f ? 1 :(android.media.AudioTrack.getMinVolume() == 0.0f ? 0 : -1))))) - 1, (i6 & 5) + (i6 | 5), getHeaders((char) ((36367 - (~android.graphics.Color.red(0))) - 1), (1151 - ((-android.view.View.getDefaultSize(0, 0)))) - 1, ((i7 | 12) << 1) - (i7 ^ 12)),getHeaders((char) (android.util.TypedValue.complexToFraction(0, 0.0f, 0.0f) > 0.0f ? 1 :(android.util.TypedValue.complexToFraction(0, 0.0f, 0.0f) == 0.0f ? 0 : -1)), ((i8 |1162) << 1) - (i8 ^ 1162), 17 - android.view.View.resolveSize(0, 0)), getHeaders((char) (5939 - (~android.widget.ExpandableListView.getPackedPositionGroup(0))) - 1), 1180 -(android.util.TypedValue.complexToFloat(0) > 0.0f ? 1 : (android.util.TypedValuecomplexToFloat(0) == 0.0f ? 0 : -1)), 21 - android.text.TextUtils.indexOf("", "")),getHeaders((char) android.text.TextUtils.getTrimmedLength(""), i10, ((i11 | 16) << 1) -(i11 ^ 16)), getHeaders((char) (android.view.ViewConfiguration.getEdgeSlop() >> 16),(i12 & 1217) + (i12 | 1217), 25 - (android.view.ViewConfiguration.getFadingEdgeLength()>> 16)), getHeaders((char) ((i13 & 55906) + (i13 | 55906)), i15, (i16 & 13) + (i16 | 13)};
    int i17 = -(android.os.Process.myPid() >> 22);
    int i18 = -android.text.TextUtils.indexOf("", "", 0, 0);
    char c = (char) ((59248 - (~(-android.graphics.Color.red(0)))) - 1);
    int edgeSlop = android.view.ViewConfiguration.getEdgeSlop() >> 16;
    int i19 = (edgeSlop ^ 1285) + ((edgeSlop & 1285) << 1);
    int i20 = -(android.os.SystemClock.elapsedRealtime() > 0 ? 1 : (android.os.SystemClockelapsedRealtime() == 0 ? 0 : -1));
    int threadPriority = android.os.Process.getThreadPriority(0);
    int i21 = -android.text.TextUtils.lastIndexOf("", '0');
    int i22 = -(android.os.Process.myTid() >> 22);
    int i23 = -(-(android.os.SystemClock.currentThreadTimeMillis() > -1 ? 1 : (android.osSystemClock.currentThreadTimeMillis() == -1 ? 0 : -1)));
    int i24 = -(-android.text.TextUtils.indexOf("", '0', 0, 0));
    method = new java.lang.String[]{getHeaders((char) ((i17 ^ 35006) + ((i17 & 35006) << 1), 1255 - android.text.TextUtils.getCapsMode("", 0, 0), 7 - (android.widgetExpandableListView.getPackedPositionForGroup(0) > 0 ? 1 : (android.widgetExpandableListView.getPackedPositionForGroup(0) == 0 ? 0 : -1))), getHeaders((char)android.view.View.getDefaultSize(0, 0), (1261 - (~(-android.text.TextUtils.lastIndexO("", '0', 0)))) - 1, (i18 & 11) + (i18 | 11)), getHeaders((char) (android.viewViewConfiguration.getFadingEdgeLength() >> 16), (android.view.ViewConfigurationgetDoubleTapTimeout() >> 16) + 1273, (android.os.Process.myTid() >> 22) + 12), getHeader(c, i19, (i20 ^ 13) + ((i20 & 13) << 1)), getHeaders((char) android.view.ViewMeasureSpec.makeMeasureSpec(0, 0), (1298 - (~(-(android.view.ViewConfigurationgetZoomControlsTimeout() > 0 ? 1 : (android.view.ViewConfiguration.getZoomControlsTimeou() == 0 ? 0 : -1))))) - 1, ((((threadPriority | 20) << 1) - (threadPriority ^ 20)) >> 6)+ 11), getHeaders((char) ((i21 ^ -1) + ((i21 & -1) << 1)), (1308 - (~(-android.graphicsdrawable.Drawable.resolveOpacity(0, 0)))) - 1, ((i22 | 5) << 1) - (i22 ^ 5)), getHeaders(char) ((i23 & 53763) + (i23 | 53763)), (1313 - (~(-android.text.TextUtilsgetTrimmedLength("")))) - 1, (i24 & 5) + (i24 | 5))};
    long[] jArr7 = { ... };
    put = jArr7;
    head = new setWillNotDraw.getHeaders(40, jArr4, 0, 0);
    get = new setWillNotDraw.getHeaders(20, jArr2, 0, 0);
    delete = new setWillNotDraw.getHeaders(60, jArr6, 0, 0);
    post = new setWillNotDraw.getHeaders(40, jArr3, 5, Long.MAX_VALUE);
    AGResponse = new setWillNotDraw.getHeaders(20, jArr, 5, Long.MAX_VALUE);
    new setWillNotDraw.getHeaders(60, jArr5, 5, Long.MAX_VALUE);
    headersMultiMap = new setWillNotDraw.getHeaders(150, jArr7, 5, 1073741823);
    int i25 = values;
    int i26 = statusCode;
}
```

Let's add pure function inlining by tracing `invoke-static` instruction. This expressions uses atmost 6 bytes, or 3 words and looks like this. [1]

```smali
invoke-static {vC, vD, vE, vF, vG}, method_id@BBBB
invoke-static/range {vCCCC .. vNNNN}, method_id@BBBB
```

Here's the way I did it:
1. Get function arguments, second argument is a method signature
2. Get function return type by its signature
3. Iterate over all argument registers, check if register exists and constant evaluated. Don't handle register pair as two arguments, treat only first one.
4. Generate driver target

Result
```java
static {
    char[] cArr = new char[1317];
    java.nio.ByteBuffer.wrap("...".getBytes("ISO-8859-1")).asCharBuffer().get(cArr, 0, 1317);
    getStatusCode = cArr;
    long[] jArr = { ... };
    getMethod = jArr;
    long[] jArr2 = { ... };
    getHeaders = jArr2;
    long[] jArr3 = { ... };
    getBody = jArr3;
    long[] jArr4 = { ... };
    AGRequest$Builder = jArr4;
    long[] jArr5 = { ... };
    addHeader = jArr5;
    long[] jArr6 = { ... };
    newBuilder = jArr6;
    char c = (char) 5939;
    int i = 1180;
    long[] jArr7 = { ... };
    put = jArr7;
    head = new setWillNotDraw.getHeaders(40, jArr4, 0, 0);
    get = new setWillNotDraw.getHeaders(20, jArr2, 0, 0);
    delete = new setWillNotDraw.getHeaders(60, jArr6, 0, 0);
    post = new setWillNotDraw.getHeaders(40, jArr3, 5, Long.MAX_VALUE);
    AGResponse = new setWillNotDraw.getHeaders(20, jArr, 5, Long.MAX_VALUE);
    new setWillNotDraw.getHeaders(60, jArr5, 5, Long.MAX_VALUE);
    headersMultiMap = new setWillNotDraw.getHeaders(150, jArr7, 5, 1073741823);
    int i2 = values;
    int i3 = statusCode;
}
```
<!--
Don't mind unsimplified calculation for unused variables, it's time to get rid of it and also handle specific pure virtual calls by tracing `invoke-virtual`.
-->
## References

  1. [Dalvik bytecode](https://source.android.com/devices/tech/dalvik/dalvik-bytecode)
  2. [Dalvik executable format](https://source.android.com/devices/tech/dalvik/instruction-formats)
  3. [Dalvik opcodes](http://pallergabor.uw.hu/androidblog/dalvik_opcodes.html)
  4. [A generic approach to automatic deobfuscation of executable code](https://www.sysnet.ucsd.edu/~bjohanne/assets/papers/2015oakland.pdf)
  5. [A Taxonomy of Obfuscating Transformations](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.68.2651&rep=rep1&type=pdf)
  6. [Evading Virus Detection Using Code Obfuscation](https://link.springer.com/chapter/10.1007/978-3-642-17569-5_39)
  7. [CodeMatch: obfuscation won't conceal your repackaged app](https://dl.acm.org/doi/abs/10.1145/3106237.3106305)
  8. [Obfuscation-Resilent Privacy Leak Detection for Mobile Apps Through Differential Analysis](https://publik.tuwien.ac.at/files/publik_278933.pdf)
  9. [Static disassembly of obfuscated binaries](https://www.usenix.org/legacy/event/sec04/tech/full_papers/kruegel/kruegel_html/)